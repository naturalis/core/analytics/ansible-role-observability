# ansible-role-observability

Ansible role to install and configure observability tools on a Linux host.

Specifically the role will:

- Install [OpenTelemetry Collector](https://opentelemetry.io/docs/collector/)
- Install exporters
- Configure OpenTelemetry Collector to receive metrics from a selection of
  locally running Prometheus exporters
- Configure OpenTelemetry Collector to receive logs from journald
- Configure OpenTelemetry Collector to receive telemetry data from other
  sources
- Configure specific node_exporter to include and / or exclude collectors
- Configure OpenTelemetry Collector to process and export telemetry data to
  other OpenTelemetry Collectors acting as gateway

## Prerequisites


### Ansible modules
To use this role make sure boto3 and botocore python modules are installed in
your Ansible environment:

```
pip install "boto3>=1.26.0"
pip install "botocore>=1.29.0"
pip install "fqdn>=1.5.1"
```

### Ansible collections

The following collections need to be present on your system or in your ./collections folder. Add this to the requirements.yml file

```yaml
collections:
  - name: prometheus.prometheus
    version: 0.19.0
  - name: amazon.aws
    version: 8.2.1
```

To make sure the are installed within the repository add this to `ansible.cfg`

```ini
[defaults]
collections_path = ./collections
```

And add `collections` to you .gitignore

## Blackbox targets for http and icmp

Currently this role can create blackbox targets when `observability_host_check` is set or `observability_check_urls` has entries.

```yaml
observability_check_urls:  # URLs to monitor from the observability stack
  - https://mysite.naturalis.com
observability_host_check: true  # enable to monitor availability of host

# Required for either observability_check_urls or observability_host_check
observability_blackbox_bucket_aws_secret_key: "{{ vault_tenant_secrets.observability.blackbox_credentials_aws_secret_access_key }}"
observability_blackbox_bucket_aws_access_key_id: "{{ vault_tenant_secrets.observability.blackbox_credentials_aws_access_key_id }}"
observability_blackbox_aws_region: "{{ vault_tenant_secrets.observability.blackbox_aws_region }}"
observability_blackbox_bucket_target: "{{ vault_tenant_secrets.observability.blackbox_bucket_target }}"
observability_blackbox_lambda_function: "{{ vault_tenant_secrets.observability.blackbox_lambda_function }}"
```

NOTE: `observability_check_urls` can only have one valid config for the entire repository. Do not create 2 different versions like for example for development and production. These will be overwritten based on the last run.

### Externally monitored URLs

This service allows for the monitoring of URLs that are externally hosted services (service that do not have an ansible accessible host). For this to work, you will need to add at least a host to your inventory. You can add a config like this:

```yaml
observability_blackbox_bucket_aws_access_key_id: secret_key_id
observability_blackbox_bucket_aws_secret_key: secret_key
observability_blackbox_aws_region: aws_region
observability_blackbox_bucket_target: target_bucket
observability_blackbox_lambda_function: lambda_function_name

observability_external_check_http: # URLs to monitor from the observability stack
  - namespace: service.name.space
    owner: email@domain.com
    team: service_team
    urls: 
      - https://yourwebsite.com
      - https://anotherwebsite.com/
```

Ansible playbook:

NOTE: This is a task that is not included in the default observability play and should be addressed directly. All needed inputs will be validated.
```yaml
---
- name: Install Observability - External URL monitoring
  hosts: all
  gather_facts: false
  tasks:
    - name: Apply observability role
      ansible.builtin.include_role:
        name: observability
        tasks_from: blackbox_external_targets
...
```

## Role Variables

In general, all variables that can be overridden are defined in `defaults/main.yml`. 

Exporter specific settings are prefixed with `prometheus_`

### Required variables

To ensure the installation of the opentelemetry agent, one of these settings should be present:
- any allowed exporter (see `defaults/main.yml` aanything prefixed with `prometheus_`, e.g. `prometheus_node_exporter: true`)
- logging is enabled: `otelcol_receiver_journald: true`
- a custom receiver is defined. See `Custom opentelemetry configurations` below in this document

### OpenTelemetry Collector

There are a few variables for installing and configuring OpenTelemetry
Collector itself.

OpenTelemetry Collector is available in different distribution. At the moment
this role will install the
[Contrib](https://github.com/open-telemetry/opentelemetry-collector-releases/blob/main/distributions/otelcol-contrib)
distribution.

To configure the version of OpenTelemetry Collector to be installed:

```yaml
otelcol_version: 0.108.0
```

In order to troubleshoot Collector and exporter issues, you can configure debug
settings:

```yaml
otelcol_debug: false
otelcol_debug_verbosity: basic
```

This role assumes that the OpenTelemetry Collector will send telemetry data to
another OpenTelemetry Collector acting as
[gateway](https://opentelemetry.io/docs/collector/deployment/gateway/). These
gateways are assumed to accept gRPC messages on port 443 with TLS and (for the
moment) basic authentication enabled (either natively or using a reverse
proxy). See
[docker-compose-otelcol-gateway](https://gitlab.com/naturalis/core/analytics/docker-compose-otelcol-gateway)
for an example. 

#### Credentials and gateways

To get credentials to authenticate with the opentelemetry gateway, you need a valid token from keycloak. To be able to retrieve these, please make sure you have a client_id and client_secret available as well as the token url. 

Configure otelcol_client_id, otelcol_client_secret, otelcol_token_url and gateway for OIDC authentication with the gateway
collectors for either logs or metrics:

```yaml
otelcol_client_id: client_id
otelcol_client_secret: secretpassword
otelcol_token_url: https://token_url
```

The gateways need to be set to deliver either metrics or logs to the observability stack. Configure these when sending either logs or metrics, depending on your needs.

```yaml
# When sending metrics
otelcol_metrics_gateway: <fqdn>
# When sending logs
otelcol_logs_gateway: <fqdn>
```

#### Service information
To add attributes to the telemetry data, we use resource attributes conforming
to the [OpenTelemetry Semantic Conventions](https://opentelemetry.io/docs/specs/semconv/).

You specify them by defining the proper service inputs like this. All are mandatory

```yaml
service:
  namespace: naturalis.core.test    # Gitlab project namespace (mandatory)
  owner: Owner@exampledomain.org    # Service owner by email address (mandatory)
  team: infra                       # Service team (mandatory)
  name: database                    # Service from group_var (defaults to app)
  environment: development          # Set deploy environment (mandatory)
```

### Receivers 

In OpenTelemetry Collector receivers collect telemetry from one or more
sources. You can configure receivers with variables documented below.

This role distinguishes between telemetry data about a host and external
telemetry data that is not related to the host on which the collector runs.

#### Logs

##### Journald
Enable receiving host related logs from journald:

```yaml
otelcol_receiver_journald: true
```

Enable sending logs to SIEM/SOC pipeline
```yaml
otelcol_receiver_siemsoc_logs: true 
```

Configure another directory where journald files are stored:

```yaml
otelcol_receiver_journald_directory: "/var/log/journal"
```

Configure systemd units and priority of logs to receive:

```yaml
otelcol_receiver_journald_prio: info
otelcol_receiver_journald_units:
  - ssh # default
  - docker # default
```

##### Windows Event logs

To receive events from a specific event log, you should set the following:

```yaml
otelcol_receiver_windows_events: true
otelcol_receiver_windows_eventlogs:
  - system # default
  - application
```

#### Metrics

Available exporters, set to true if you want it installed. More exporters to come

```yaml
prometheus_node_exporter: false
prometheus_node_exporter_external: false
prometheus_textfile_collector: false
prometheus_windows_exporter: false
prometheus_mysqld_exporter: false
prometheus_postgres_exporter: false
prometheus_nginx_exporter: false
prometheus_nginxlog_exporter: false  # Does not install an exporter, needs to be preinstalled
```

To enable or disable OpenTelemetry Collector to receive / scrape data from an
exporter change the relevant variables. If your system has a exporter installed in some other way, you can set the `_preinstalled` setting to true and that exporter will not be installled as part of the otel agent deployment. 

It is highly recommended to let this role install the exporters 

```yaml
prometheus_node_exporter: true # This will install node_exporter as well
prometheus_node_exporter_preinstalled: false # Set this to true if you don't want this role to install the exporter
```

All exporters are assumed to run locally and listen on localhost (`127.0.0.1`)
and the default port for the exporter.

These exporters are host related:

- node_exporter
- postgres_exporter
- mysqld_exporter
- nginxlog_exporter
- nginx_exporter
- uwsgi_exporter
- postfix_exporter
- memcached_exporter
- redis_exporter
- windows_exporter (Windows only)

These exporters are assumed to export metrics about external systems:

- node_exporter_external
- helmet_exporter
- ipmi_exporter
- modbus_exporter

For external exporters you have to provide target files in
`/etc/otelcol/targets/<name_exporter>_something.yml`. These target files should
include required parameters.

#### Node exporter

These are the configurations that can be passed to install the node_exporter:

```yaml
prometheus_node_exporter: true
prometheus_node_exporter_preinstalled: false
prometheus_node_exporter_version: 1.8.0
prometheus_node_exporter_web_listen_address: "localhost:9100"
prometheus_node_exporter_textfile_dir: "/var/lib/node_exporter"
prometheus_node_exporter_enabled_collectors:
  - systemd
  - textfile:
      directory: "{{ prometheus_node_exporter_textfile_dir }}"
prometheus_node_exporter_disabled_collectors: []
```
#### Textfile collector

Scripts that creates a .prom file which will be scraped by the node exporter.

Required inputs:
```yaml
prometheus_textfile_collector: true
```

By default the apt_info script found [here](https://github.com/prometheus-community/node-exporter-textfile-collector-scripts/blob/master/apt_info.py) will be created. A systemd service and timer unit will be created to run the script daily. 

#### MySQLd exporter

This role requires the `community.mysql` collection version 3.9.0

Prerequisite:

The installer needs mysql credentials on the database to be able to scrape the metrics. Please make sure this user is present and you pass the credentials to the exporter config using.

```yaml
prometheus_mysqld_exporter_username: username
prometheus_mysqld_exporter_password: secret
```

Example for installation using Ansible:

```yaml
- name: Create mysql user for Observability mysqld_exporter
  community.mysql.mysql_user:
    name: username
    host: "localhost"
    password: secret
    priv: "*.*:PROCESS,REPLICATION CLIENT,SELECT"
    resource_limits:
      MAX_USER_CONNECTIONS: 3
```

Required inputs:

```yaml
prometheus_mysqld_exporter: true
prometheus_mysqld_exporter_username: username
prometheus_mysqld_exporter_password: secret
```

Optional inputs:
```yaml
prometheus_mysqld_exporter_preinstalled: false
prometheus_mysqld_exporter_web_listen_address: "localhost:9104"
```

#### Postgres exporter

Prerequisite:

The installer needs mysql credentials on the database to be able to scrape the metrics. Please make sure this user is present and you pass the credentials to the exporter config using.

Required inputs:

```yaml
prometheus_postgres_exporter: true
prometheus_postgres_exporter_username: username
prometheus_postgres_exporter_password: secret
```

Prerequisites:

The exporter needs credentials to be able to connect to the postgres instance. These should be present before running this role.

Example for installation using [ansible-role-postgresql](https://gitlab.com/naturalis/lib/ansible-role-postgresql):

```yaml
postgresql_user:
  - name: "{{ prometheus_postgres_exporter_username }}"
    password: "{{ prometheus_postgres_exporter_password }}"
    role_attr_flags: LOGIN

postgresql_membership:
  - group: pg_monitor
    target_roles: "{{ prometheus_postgres_exporter_username }}"

postgresql_privs:
  - db: postgres
    privs: CONNECT
    roles: "{{ prometheus_postgres_exporter_username }}"
    type: database
```

Defaults:
```yaml
prometheus_postgres_exporter_version: 0.16.0
prometheus_postgres_exporter_preinstalled: false
prometheus_postgres_exporter_web_listen_address: ["localhost:9187"]
prometheus_postgres_exporter_uri: localhost:5432/postgres?sslmode=disable
```

#### Nginxlog exporter

To use this role, you have to configure nginx like mentioned here: https://github.com/nginxinc/nginx-prometheus-exporter?tab=readme-ov-file#prerequisites

Required inputs:
```yaml
prometheus_nginxlog_exporter: true
prometheus_nginxlog_exporter_preinstalled: false # Set to true if you have a custom or own install already present
```

Optional inputs:
```yaml
prometheus_nginxlog_exporter_web_listen_address: "localhost:9113"
```
#### Nginx exporter

To use this role, you have to configure nginx like mentioned here: https://github.com/nginxinc/nginx-prometheus-exporter?tab=readme-ov-file#prerequisites

Prerequisites

NOTE: If you change the port or the location, please change this default setting

```yaml 
prometheus_nginx_exporter_scrape_uri: "http://localhost:8081/nginx_status"
```

Add this to you nginx configuration:

```conf
server {
  listen 8081;
  location = /nginx_status {
    stub_status;
  }
}
```

When using nginx in a docker-compose, make sure to expose the configured port to the localhost. Do not expose the port with traefik or allow any external access. For example where exporter.conf has the nginx config set like above:

```yaml
  nginx:
    image: nginx
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_nginx"
    restart: unless-stopped
    networks:
      - webserver
    volumes:
      - ./exporter.conf:/etc/nginx/conf.d/exporter.conf
    ports:
      - 8081:8081
```

Required inputs:
```yaml
prometheus_nginx_exporter: true
```

Defaults:
```yaml
prometheus_nginx_exporter_preinstalled: false # Set to true if you have a custom or own install already present
prometheus_nginx_exporter_version: 1.3.0
prometheus_nginx_exporter_web_listen_address: "localhost:9113"
prometheus_nginx_exporter_scrape_uri: "http://localhost:8081/nginx_status"
```
### Facts collector

The facts collector is a module that collects local facts about your machine and exports it using the prometheus_node_exporter textfilecollector service.

To enable the facts collector, set the required inputs to true: 
```yaml
observability_facts_collector: true
```

It is allowed to specify the enabled jobs, as long as at least one job is enabled. New jobs will be added in future releases of this role.

Defaults:
```yaml
observability_facts_collector_enabled_jobs:
  - apt_info.py
```

Currently available jobs:
  - apt_info: retrieves information about available updates

### Custom opentelemetry configurations

It is possible to enable you own receivers, processors and pipelines. To do so you will have to add the appropriate variables. Please check your complete config at https://otelbin.io or use a development environment to properly test your configuration. below are some examples 

Custom receiver
```yaml
otelcol_custom_receiver: |
  webhookevent/minio_audit:
    endpoint: 172.17.0.1:9191
    read_timeout: "500ms"
    path: "/minio_audit/receiver"
    health_path: "/minio_audit/healthcheck"
```

Custom processor:
```yaml
otelcol_custom_processor: |
  filter/siemsoc:
    logs:
      include:
        match_type: strict
        resource_attributes:
          - key: service.siemsoc_logs
            value: enabled
```

Custom exporter:
```yaml
otelcol_custom_exporter: |
  file/audit:
    path: /data/logs/audit_logs.json
    rotation:
    max_backups: 10
```

Custom pipeline:
```yaml
otelcol_custom_pipeline: |
  logs/minio_audit:
    receivers:
      - webhookevent/minio_audit
    processors:
      - batch
      - memory_limiter
      - resourcedetection/host
    exporters: [otlp/logs,file/audit]
```

## Examples Ansible

### Ubuntu observability variables

An example for Ubuntu exporting default metrics and logs 

```yaml
otelcol_client_id: client_id
otelcol_client_secret: secretpassword
otelcol_token_url: https://token_url
otelcol_metrics_gateway: metrics.gateway.analytics.naturalis.io
otelcol_logs_gateway: logs.gateway.analytics.naturalis.io
otelcol_receiver_journald: true
prometheus_node_exporter: true
```

### Windows observability variables

An example for Windows with both metrics and logs exported:

```yaml
otelcol_client_id: client_id
otelcol_client_secret: secretpassword
otelcol_token_url: https://token_url
otelcol_metrics_gateway: metrics.gateway.analytics.naturalis.io
otelcol_logs_gateway: logs.gateway.analytics.naturalis.io
otelcol_receiver_windows_events: true
otelcol_receiver_windows_eventlogs:
  - system
prometheus_windows_exporter: true
```

### Playbook

Apart setting the relevant variables as documented above, using this role is as
simple as this for Ubununt:

```yaml
- name: Configure observability
  hosts: servers
  tasks:
    - include_role:
        name: observability
```

For Windows you should use (tasks_from): 

```yaml
- name: Apply observability role 
  ansible.builtin.include_role:
    name: observability
    tasks_from: main_windows
```

## License

Apache License 2.0

## Author Information

* David Heijkamp
* Sjoerd de Jong
