---
- name: Create the otelcol group
  ansible.builtin.group:
    name: "{{ _otelcol_system_group }}"
    state: present
    system: true
  become: true
  when: _otelcol_system_group != "root"

- name: Create the otelcol user
  ansible.builtin.user:
    name: "{{ _otelcol_system_user }}"
    groups: "{{ _otelcol_system_group }}"
    append: true
    shell: /usr/sbin/nologin
    system: true
    create_home: false
    home: /
  become: true
  when: _otelcol_system_user != "root"

- name: Make otelcol user member of systemd-journal group
  ansible.builtin.user:
    name: "{{ _otelcol_system_user }}"
    groups: "systemd-journal"
    append: true
  become: true
  when: otelcol_receiver_journald

- name: Create otelcol directory
  ansible.builtin.file:
    path: "/opt/otelcol"
    state: directory
    mode: 0775
  become: true
  check_mode: false

- name: Download otelcol binary to local folder
  ansible.builtin.get_url:
    url: "https://github.com/open-telemetry/opentelemetry-collector-releases/releases/download/v{{ otelcol_version }}/otelcol-contrib_{{ otelcol_version }}_linux_{{ go_arch }}.tar.gz"
    dest: "/tmp/otelcol-contrib_{{ otelcol_version }}_linux_{{ go_arch }}.tar.gz"
    mode: '0644'
  register: _download_binary
  until: _download_binary is succeeded
  retries: 5
  delay: 2
  check_mode: false

- name: Unpack otelcol binary
  ansible.builtin.unarchive:
    src: "/tmp/otelcol-contrib_{{ otelcol_version }}_linux_{{ go_arch }}.tar.gz"
    dest: "/opt/otelcol/"
    remote_src: true
  become: true
  check_mode: false
  when: _download_binary.changed  # noqa: no-handler

- name: Symlink otelcol
  ansible.builtin.file:
    src: "/opt/otelcol/otelcol-contrib"
    dest: "{{ _binary_install_dir }}/otelcol"
    state: link
    mode: 0755
    owner: root
    group: root
  become: true
  notify: restart_otelcol
  when: not ansible_check_mode

- name: Create otelcol config directories
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    mode: 0755
  become: true
  loop:
    - "{{ _otelcol_config_path }}"
    - "{{ _otelcol_config_targets_path }}"

- name: Copy the otelcol environment file
  ansible.builtin.template:
    src: otelcol.conf.j2
    dest: /etc/otelcol/otelcol.conf
    owner: root
    group: root
    mode: 0644
  become: true
  notify: restart_otelcol

- name: Copy the OpenTelemetry Collector config file
  ansible.builtin.template:
    src: config.yaml.j2
    dest: "{{ _otelcol_config_file }}"
    validate: "{{ _otelcol_validate }}"
    owner: root
    group: root
    mode: 0644
  become: true
  notify: restart_otelcol

- name: Copy the otelcol systemd service file
  ansible.builtin.template:
    src: otelcol.service.j2
    dest: /etc/systemd/system/otelcol.service
    owner: root
    group: root
    mode: 0644
  become: true
  notify: restart_otelcol
  register: otelcol_service

- name: Enable and start OpenTelemetry Collector
  ansible.builtin.systemd:  # noqa: no-handler
    daemon_reload: true
    name: otelcol
    enabled: true
    state: started
  become: true
  when:
    - not otelcol_service.changed
...
