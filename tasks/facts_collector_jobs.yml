---
- name: Create facts collector jobs
  when: "collector_job.name in observability_facts_collector_enabled_jobs"
  block:
    - name: Install required packages
      become: true
      ansible.builtin.apt:
        name: "{{ item }}"
      loop: "{{ collector_job.required_packages }}"
      when: collector_job.required_packages is defined

    - name: Create fact collector script
      become: true
      ansible.builtin.template:
        src: "{{ collector_job.template_file }}"
        dest: "{{ _observability_facts_collector_jobs_dir }}/{{ collector_job.name }}"
        mode: "0755"

    - name: Create systemd job and timer
      become: true
      ansible.builtin.template:
        src: jobs.{{ systemd_item }}.j2
        dest: "/etc/systemd/system/{{ collector_job.name | splitext | first }}.{{ systemd_item }}"
        owner: "root"
        group: "root"
        mode: "0644"
      loop:
        - service
        - timer
      loop_control:
        loop_var: systemd_item
      register: systemd_services_timers

    - name: Enable and start timer  # noqa:no-handler
      become: true
      ansible.builtin.systemd:
        daemon_reload: true
        name: "{{ collector_job.name | splitext | first }}.timer"
        enabled: true
        state: started
      when:
        - systemd_services_timers.changed
        - not ansible_check_mode

    - name: Enable service
      become: true
      ansible.builtin.systemd:
        daemon_reload: true
        name: "{{ collector_job.name | splitext | first }}.service"
        enabled: true
      when: not ansible_check_mode

- name: Remove facts collector jobs when not enabled
  when: "collector_job.name not in observability_facts_collector_enabled_jobs"
  block:
    - name: Check for job service file
      become: true
      ansible.builtin.stat:
        path: "/etc/systemd/system/{{ collector_job.name | splitext | first }}.service"
      register: service_file

    - name: Disable service
      become: true
      ansible.builtin.systemd:
        daemon_reload: true
        name: "{{ collector_job.name | splitext | first }}.service"
        state: stopped
      when: service_file.stat.exists

    - name: Remove systemd job
      become: true
      ansible.builtin.file:
        path: "/etc/systemd/system/{{ collector_job.name | splitext | first }}.service"
        state: absent
      when: service_file.stat.exists

    - name: Check for job timer file
      become: true
      ansible.builtin.stat:
        path: "/etc/systemd/system/{{ collector_job.name | splitext | first }}.timer"
      register: service_timer_file

    - name: Disable and stop timer
      become: true
      ansible.builtin.systemd:
        daemon_reload: true
        name: "{{ collector_job.name | splitext | first }}.timer"
        state: stopped
      when: service_timer_file.stat.exists

    - name: Remove systemd timer
      become: true
      ansible.builtin.file:
        path: "/etc/systemd/system/{{ collector_job.name | splitext | first }}.timer"
        state: absent
      when: service_timer_file.stat.exists

    - name: Remove fact collector script
      become: true
      ansible.builtin.file:
        state: absent
        path: "{{ _observability_facts_collector_jobs_dir }}/{{ collector_job.name }}"
...
