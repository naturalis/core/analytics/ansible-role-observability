---
- name: Opentelemetry Agent input validation
  ansible.builtin.assert:
    that:
      - service is defined
      - service is iterable
      - service.name is defined
      - service.name is string
      - service.namespace is defined
      - service.namespace is string
      - service.owner is defined
      - service.owner is string
      - service.team is defined
      - service.team is string
      - service.environment is defined
      - service.environment is string
      - otelcol_client_id is defined
      - otelcol_client_id is string or otelcol_client_id is vault_encrypted
      - otelcol_client_secret is defined
      - otelcol_client_secret is string or otelcol_client_secret is vault_encrypted
      - otelcol_token_url is defined
      - otelcol_token_url is string or otelcol_token_url is vault_encrypted
    fail_msg: >
      "Make sure all service related information is present"

- name: Opentelemetry Agent service.owner contact validation
  ansible.builtin.assert:
    that:
      - service.owner is match('(?i)^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$')
    fail_msg: >
      "Make sure owner contains a valid email address"

- name: Opentelemetry Metrics input validation
  ansible.builtin.assert:
    that:
      - otelcol_metrics_gateway is defined
      - otelcol_metrics_gateway is string
    fail_msg: >
      "Make sure 'otelcol_metrics_gateway' is set"
    quiet: true
  when: _otelcol_metrics

- name: Opentelemetry Logs credentials validation
  ansible.builtin.assert:
    that:
      - otelcol_logs_gateway is defined
      - otelcol_logs_gateway is string
    fail_msg: >
      "Make sure 'otelcol_logs_gateway' is set"
    quiet: true
  when: _otelcol_logs

- name: Blackbox AWS credentials present
  ansible.builtin.assert:
    that:
      - observability_blackbox_bucket_aws_access_key_id is defined
      - observability_blackbox_bucket_aws_access_key_id is string or observability_blackbox_bucket_aws_access_key_id is vault_encrypted
      - observability_blackbox_bucket_aws_secret_key is defined
      - observability_blackbox_bucket_aws_secret_key is string or observability_blackbox_bucket_aws_secret_key is vault_encrypted
      - observability_blackbox_bucket_target is defined
      - observability_blackbox_bucket_target is string or observability_blackbox_bucket_target is vault_encrypted
      - observability_blackbox_aws_region is defined
      - observability_blackbox_aws_region is string or observability_blackbox_aws_region is vault_encrypted
      - observability_blackbox_lambda_function is defined
      - observability_blackbox_lambda_function is string or observability_blackbox_lambda_function is vault_encrypted
      - ansible_host is community.general.fqdn_valid or ansible_host is ansible.utils.ip_address
    fail_msg: >
      Make sure `observability_blackbox_bucket_aws_access_key_id`,
      `observability_blackbox_bucket_aws_secret_key`,
      `observability_blackbox_bucket_target`,
      `observability_blackbox_aws_region` and
      `observability_blackbox_lambda_function` are set and
      `ansible_host` is a valid FQDN or IP address"
    quiet: true
  when: _observability_host_check or _observability_check_urls

- name: AWS environment variables absence validation
  run_once: true
  delegate_to: localhost
  vars:
    aws_security_token: "{{ lookup('ansible.builtin.env', 'AWS_SECURITY_TOKEN') }}"
    aws_session_token: "{{ lookup('ansible.builtin.env', 'AWS_SESSION_TOKEN') }}"
  ansible.builtin.assert:
    that:
      - aws_security_token | length < 1
      - aws_session_token | length < 1
    fail_msg: >
      Either AWS_SECURITY_TOKEN or AWS_SESSION_TOKEN is set in your environment.
      Please unset these before continuing.
      Use `unset AWS_SECURITY_TOKEN` or `unset AWS_SESSION_TOKEN` before running this play
  when: _observability_host_check or _observability_check_urls

- name: Check for validity of URL's
  run_once: true
  delegate_to: localhost
  ansible.builtin.assert:
    that:
      - item is uri(schemes=['http', 'https'])
    quiet: true
  loop: "{{ observability_check_urls }}"
  when: _observability_check_urls

- name: Postgres exporter input validation
  ansible.builtin.assert:
    that:
      - prometheus_postgres_exporter_uri is defined
      - prometheus_postgres_exporter_uri is string
      - prometheus_postgres_exporter_username is defined
      - prometheus_postgres_exporter_username is string
      - prometheus_postgres_exporter_password is string or prometheus_postgres_exporter_password is vault_encrypted
      - prometheus_postgres_exporter_version is defined
      - prometheus_postgres_exporter_version is string
      - prometheus_postgres_exporter_web_listen_address is defined
      - prometheus_postgres_exporter_web_listen_address is string
    fail_msg: >
      "Make sure all required inputs for using the Postgres exporter are present"
  when: prometheus_postgres_exporter

- name: Observability Facts collector input validation
  ansible.builtin.assert:
    that:
      - prometheus_node_exporter is true
      - observability_facts_collector_enabled_jobs is defined
      - observability_facts_collector_enabled_jobs is iterable
      # - observability_facts_collector_enabled_jobs | length > 0
    fail_msg: >
      "Make sure all required inputs for using the Observability fact collector are present"
  when: observability_facts_collector

- name: Observability Facts collector enabled jobs input validation
  ansible.builtin.assert:
    that:
      - collector_job.name is defined
      - collector_job.name is string
      - collector_job.template_file is defined
      - collector_job.template_file is string
      - collector_job.schedule is defined
      - collector_job.schedule is string
    fail_msg: >
      "Make sure all required inputs for enabled jobs in the Observability fact collector are present"
  loop: "{{ _observability_facts_collector_jobs }}"
  loop_control:
    loop_var: collector_job
  when:
    - observability_facts_collector
    - collector_job.name in observability_facts_collector_enabled_jobs
...
